package com.exemplo.sqlite.model;

import java.io.Serializable;

public class AlunoModel implements Serializable{
	
	/** serialVersionUID **/
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String nome;
	private String email;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override 
	public String toString(){
		return this.nome;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o != null && o instanceof AlunoModel) {
			AlunoModel aluno = (AlunoModel)o;
			if(aluno.getId() != null && aluno.getId().equals(this.getId())){
				return true;
			}
		}
		return false;
		
	}
	
}
