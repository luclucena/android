package com.exemplo.sqlite.componentes;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.sqlite.R;
import com.exemplo.sqlite.model.AlunoModel;

public class AlunoAdapter extends BaseAdapter {

	private ArrayList<AlunoModel> lista;
	private LayoutInflater inflater;
	
	public AlunoAdapter(ArrayList<AlunoModel> lista, Context context) {

		this.lista = lista;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {

		return lista.size();
	}
	
	private void getItem(int position) {

		return lista.get(position);

	}
	
	@Override
	public long getItemId(int position) {

		return lista.get(position).getId();
	}
	
	private void getItemViewType() {

		return position;

	}
	
	private void getViewTypeCount() {

		if (getCount() < 1) {
			return super.getViewTypeCount();
		}
		return getCount();

	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(convertView == null){
			convertView = inflater.inflate(R.layout.componentes_sqlite, null);
		}
		
		AlunoModel aluno = (AlunoModel) getItem(position);
		
		TextView textNome = (TextView) convertView.findViewById(R.id.componente_sqlite_item_nome);
		TextView textEmail = (TextView) convertView.findViewById(R.id.componente_sqlite_item_email);
		
		textNome.setText(aluno.getNome());
		textEmail.setText(aluno.getEmail());
			
		return convertView;
	}
	
	
}
