package com.example.sqlite;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity implements OnClickListener {

	private EditText editNome, editEmail;
	private Button botaoIncluir, botaoAlterar, botaoExcluir;
	private ListView listViewLista;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		inicializarComponentes();
	}

	private void inicializarComponentes(){
		editNome = (EditText) findViewById(R.id.sqlite_editnome);
		editEmail = (EditText) findViewById(R.id.sqlite_editemail);
		
		botaoIncluir = (Button) findViewById(R.id.sqlite_button_incluir);
		botaoIncluir.setOnClickListener(this);
		
		botaoAlterar = (Button) findViewById(R.id.sqlite_button_atualizar);
		botaoAlterar.setOnClickListener(this);
		
		botaoExcluir = (Button) findViewById(R.id.sqlite_button_excluir);
		botaoExcluir.setOnClickListener(this);
		
		listViewLista = (ListView) findViewById(R.id.sqlite_listview);
		
		//TODO PARA TESTE
		lista = new ArrayList<AlunoModel>
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		
		switch(){
		case
		
		break;
		}
		
	}
}
