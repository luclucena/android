package com.example.helloworld;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {	
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    }
    
    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
    	super.onActivityResult(arg0, arg1, arg2);
    	
    	if (arg1 == RESULT_OK) {
    		Toast.makeText(this, "Click Sim", Toast.LENGTH_LONG).show();
    	} else {
    		Toast.makeText(this, "Click Nao", Toast.LENGTH_LONG).show();
    	}
    	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        
        switch(id) {
        	case R.id.action_1 :
        		Intent intent1 = new Intent(MainActivity.this, MainActivityIndex.class);
				startActivity(intent1);
        		break;
        	
        	case R.id.action_2 :
        		Intent intent2 = new Intent(MainActivity.this, SegundaTelaActivity.class);
				startActivity(intent2);
        		break;
        	
        	case R.id.action_3 :
        		break;
        }
        
        //if (id == R.id.action_settings) {
        //    return true;
        //}
        return super.onOptionsItemSelected(item);
    }
}
