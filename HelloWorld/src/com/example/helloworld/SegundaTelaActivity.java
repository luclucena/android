package com.example.helloworld;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SegundaTelaActivity extends ActionBarActivity implements OnClickListener {

	private Button btPreview;
	private Button btNext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_segunda_tela);
		
		btPreview = (Button) findViewById(R.id.bt_yes);
		btPreview.setOnClickListener(this);
		
		btNext = (Button) findViewById(R.id.bt_no);
		btNext.setOnClickListener(this);
			
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bt_preview :
			setResult(RESULT_OK);
			finish();
			break;
		
		case R.id.bt_next:
			setResult(RESULT_CANCELED);
			finish();
			break;
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.segunda_tela, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		//int id = item.getItemId();
		
		return super.onOptionsItemSelected(item);
	}
	
}
