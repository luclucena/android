package com.exemple.minhascompras.models;

import java.io.Serializable;

public class ListViewModel implements Serializable {

	/** serialVersionUID **/
	private static final long serialVersionUID = -3459013137400731913L;	
	
	private Long image;
	private String text;
	
	public Long getImage() {
		return image;
	}
	public void setImage(Long image) {
		this.image = image;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	
}
