package com.exemple.minhascompras.models;

import java.io.Serializable;

public class FuncionarioModel implements Serializable{

	/** serialVersionUID**/
	private static final long serialVersionUID = 5479089752077053330L;


	private String nome;
	private String eid;
	private String situacao;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEid() {
		return eid;
	}
	public void setEid(String eid) {
		this.eid = eid;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	
	
}
