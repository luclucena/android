package com.exemple.minhascompras.componets;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.minhascompras.R;
import com.exemple.minhascompras.models.FuncionarioModel;

public class FuncionarioAdapter extends BaseAdapter {

	private ArrayList<FuncionarioModel> lista;
	private LayoutInflater inflater;

	public FuncionarioAdapter(ArrayList<FuncionarioModel> lista, Context context) {
		this.lista = lista;
		this.inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		if (getCount() < 1) {
			return super.getViewTypeCount();
		}
		return getCount();
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(com.example.minhascompras.R.layout.component_funcionario_item, null);
		}
		FuncionarioModel funcionario = (FuncionarioModel) getItem(position);
		

		TextView nome = (TextView) convertView
				.findViewById(R.id.component_funcionario_textview_nome);
		TextView eid = (TextView) convertView
				.findViewById(R.id.component_funcionario_textview_eid);
		TextView situacao = (TextView) convertView
				.findViewById(R.id.component_funcionario_textview_situacao);

		nome.setText(funcionario.getNome());
		eid.setText(funcionario.getEid());
		situacao.setText(funcionario.getSituacao());

		return convertView;
	}
	
	public void setLista(ArrayList<FuncionarioModel> lista){
		this.lista = lista;
		notifyDataSetChanged();
	}

}
