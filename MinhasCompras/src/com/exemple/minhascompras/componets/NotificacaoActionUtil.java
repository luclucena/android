package com.exemple.minhascompras.componets;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class NotificacaoActionUtil {

	public static final int NOTIFICATION_REQUEST_CODE = 123;
	
	public static void create(Context context, CharSequence ticker, CharSequence titulo, 
			CharSequence mensagem, int icone, int id, Intent intent){
		
		Notification notificacao = null;
		PendingIntent pendingIntent = PendingIntent.getActivity(context, NOTIFICATION_REQUEST_CODE, intent, 0);
		
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			Notification.Builder construtor = new Notification.Builder(context);
			construtor.setContentTitle(titulo);
			construtor.setContentText(mensagem);
			construtor.setSmallIcon(icone);
			construtor.setContentIntent(pendingIntent);
			
			
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
				notificacao = construtor.build();
			} else {
				notificacao = construtor.getNotification();
			}
			
		} else {
			notificacao = new Notification(icone, ticker, System.currentTimeMillis());
			notificacao.setLatestEventInfo(context, titulo, mensagem, pendingIntent);
		}
		
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificacao.flags |= Notification.FLAG_AUTO_CANCEL;
		manager.notify(id, notificacao);
	}
	
}
