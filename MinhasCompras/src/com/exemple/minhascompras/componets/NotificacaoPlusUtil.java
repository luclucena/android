package com.exemple.minhascompras.componets;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class NotificacaoPlusUtil {
	
	private static final int REQUEST_CODE_ACAO1 = 4321;
	private static final int REQUEST_CODE_ACAO2 = 43210;
	private static final int REQUEST_CODE_ACAO3 = 543210;
	
	public static void create(Context context, 
			CharSequence ticker, CharSequence titulo, int icone, int id, Intent intent1, Intent intent2, Intent intent3){
		
		Notification notificacao = null;
		int apiVersion = Build.VERSION.SDK_INT;
		PendingIntent pending1 = PendingIntent.getActivity(context, REQUEST_CODE_ACAO1, intent1, 0);
		PendingIntent pending2 = PendingIntent.getActivity(context, REQUEST_CODE_ACAO2, intent2, 0);
		PendingIntent pending3 = PendingIntent.getActivity(context, REQUEST_CODE_ACAO3, intent3, 0);
		
		if (apiVersion >= Build.VERSION_CODES.HONEYCOMB) {
			
			Notification.Builder construtor = new Notification.Builder(context);
			construtor.setContentTitle(titulo);
			construtor.setSmallIcon(icone);
			construtor.setStyle(new Notification.BigTextStyle().bigText("Aqui eu posso escrever um texto qualquer independente do tamanho e quantidade de caracteres, pois esse style me permite tal coisa. Se quiser comprovar, escreva mais."));
			construtor.addAction(icone, "Acao1", pending1);
			construtor.addAction(icone, "Acao2", pending2);
			construtor.addAction(icone, "Acao3", pending3);
			
			if (apiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
				notificacao = construtor.build();
			} else {
				notificacao = construtor.getNotification();
			}
			
		} else {
			notificacao = new Notification(icone, ticker, System.currentTimeMillis());
			notificacao.setLatestEventInfo(context, "FALHA", "Notificacao valida apenas para versoes maiores que 11", pending1);
		}
		
		NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificacao.flags |= Notification.FLAG_AUTO_CANCEL;
		manager.notify(id, notificacao);
	}
	
}
