package com.exemple.minhascompras.componets;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhascompras.R;
import com.exemple.minhascompras.models.ListViewModel;

public class ListViewAdapter extends BaseAdapter {
	
	private ArrayList<ListViewModel> lista;
	private LayoutInflater inflater;
	
	
	public ListViewAdapter(ArrayList<ListViewModel> lista, Context context) {
		this.lista = lista;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(int position) {
		return lista.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView != null) {
			return convertView;
		}
		
		ListViewModel item = (ListViewModel) getItem(position);
		convertView = inflater.inflate(R.layout.component_listview, null);
		
		ImageView image = (ImageView) convertView.findViewById(R.id.image_icon);
		TextView text = (TextView) convertView.findViewById(R.id.text_description);
		
		image.setImageResource(item.getImage().intValue());
		text.setText(item.getText());
		
		return convertView;
	}
	
	@Override
	public int getItemViewType(int position) {
		return position;
	}
	
	@Override
	public int getViewTypeCount() {
		return getCount();
	}
}
