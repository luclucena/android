package com.example.minhascompras;

import java.util.ArrayList;

import com.exemple.minhascompras.componets.FuncionarioAdapter;
import com.exemple.minhascompras.models.FuncionarioModel;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

public class SharedPreferencesActivity extends Activity implements OnClickListener {

	private static final String PREFRENCES_NAME = "checked";
	private CheckBox check;
	private Button salvar;
	private SharedPreferences sharedPreferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shared_preferences);
		sharedPreferences = getSharedPreferences(PREFRENCES_NAME, MODE_PRIVATE);
		
		check = (CheckBox) findViewById(R.id.sharedpreferences_checkbox);
		salvar = (Button) findViewById(R.id.sharedpreferences_salvar);
		salvar.setOnClickListener(this);
		
		boolean arquivo1 = sharedPreferences.getBoolean("arquivo1", false);
		check.setChecked(arquivo1);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.sharedpreferences_salvar) {
			sharedPreferences.edit().putBoolean("arquivo1", check.isChecked()).commit();
		}
        
	}

	
}
