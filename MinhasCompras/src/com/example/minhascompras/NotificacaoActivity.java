package com.example.minhascompras;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.exemple.minhascompras.componets.NotificacaoActionUtil;
import com.exemple.minhascompras.componets.NotificacaoPlusUtil;

public class NotificacaoActivity extends Activity implements android.view.View.OnClickListener {

	private Button botaoSimples;
	private Button botaoPlus;
	private Button botaoPeriodico;
	
	private static int ID_NOTIFICACAO_SIMPLES = 111;
	private static int ID_NOTIFICACAO_PLUS = 222;
	private static int ID_NOTIFICACAO_PERIODICA = 333;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notificacao);
		
		inicializarComponentes();
	}
	
	private void inicializarComponentes(){
		botaoSimples = (Button) findViewById(R.id.notificacaoactivity_button_notificacao);
		botaoSimples.setOnClickListener(this);
		botaoPlus = (Button) findViewById(R.id.notificacaoactivity_button_notificacao_plus);
		botaoPlus.setOnClickListener(this);
		botaoPeriodico = (Button) findViewById(R.id.notificacaoactivity_button_notificacao_temporizado);
		botaoPeriodico.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		int icone = android.R.drawable.ic_lock_idle_alarm;
		String ticker = "Notificacoes";
		Intent intent;
		
		switch (v.getId()) {
			case R.id.notificacaoactivity_button_notificacao:
				intent = new Intent(this, AcaoAActivity.class);
				NotificacaoActionUtil.create(this, ticker, "Notificacao Simples",
						"Clique para chamar Activity", icone, ID_NOTIFICACAO_SIMPLES, intent);
				break;
				
			case R.id.notificacaoactivity_button_notificacao_plus:
				intent = new Intent(this, AcaoAActivity.class);
				Intent intentB = new Intent(this, AcaoBActivity.class);
				Intent intentC = new Intent(this, AcaoCActivity.class);
				
				NotificacaoPlusUtil.create(this, ticker, "Notificacao Plus", 
						R.drawable.ic_launcher, ID_NOTIFICACAO_PLUS, intent, intentB, intentC);
				break;
				
			case R.id.notificacaoactivity_button_notificacao_temporizado:
				break;
		}
		
	}

}
