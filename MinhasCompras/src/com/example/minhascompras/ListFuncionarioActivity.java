package com.example.minhascompras;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.exemple.minhascompras.componets.FuncionarioAdapter;
import com.exemple.minhascompras.models.FuncionarioModel;

public class ListFuncionarioActivity extends Activity implements OnClickListener, android.content.DialogInterface.OnClickListener {

	private static final String URL_PESQUISA = "http://10.42.4.83:8088/academico/rest/academico/pesquisaFuncionario/{nome}/null/null/{eid}/null/null"
;
			
	
	private ArrayList<FuncionarioModel> listaFuncionarioModels;
	private FuncionarioAdapter adapter;
	private ListView listViewFuncionario;
	private EditText editNome, editEID;
	private Button botaoPesquisar;
	private ProgressDialog progressDialog;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_funcionarios);
		inicializarComponentes();
	}
	
	private void inicializarComponentes() {
		listViewFuncionario = (ListView) findViewById(R.id.listafuncionario_listview_lista);
		editNome = (EditText) findViewById(R.id.listafuncionario_edittext_nome);
		editEID = (EditText) findViewById(R.id.listafuncionario_edittext_eid);
		botaoPesquisar = (Button) findViewById(R.id.listafuncionario_button_pesquisarr);
		adapter = new FuncionarioAdapter(new ArrayList<FuncionarioModel>(), this);
		
		botaoPesquisar.setOnClickListener(this);
		listViewFuncionario.setAdapter(adapter);
		
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage(getString(R.string.mensagem_progress_dialog));
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.listafuncionario_button_pesquisarr) {
			
			if (editNome.getText().toString().trim().length() == 0 && editEID.getText().toString().trim().length() == 0) {
				AlertDialog.Builder construtorDialog = new AlertDialog.Builder(this);
				construtorDialog.setMessage("Deseja buscar todos os funcionarios na base de dados?");
				construtorDialog.setTitle("ATENCAO");
				construtorDialog.setCancelable(false);
				construtorDialog.setPositiveButton("Sim", this);
				construtorDialog.setNegativeButton("Nao", this);
				
				AlertDialog dialog = construtorDialog.create();
				dialog.show();
				return;
			}
			iniciaBusca();
			
		}
		
	}
	
	private void iniciaBusca() {
		String nome = editNome.getText().toString();
		String eid = editEID.getText().toString();
		
		String urlBusca = URL_PESQUISA;
		
		if (nome.length() > 0){
			urlBusca = urlBusca.replace("{nome}", nome);
			
		} else {
			urlBusca = urlBusca.replace("{nome}", "null");
		}
		
		if (eid.length() > 0){
			urlBusca = urlBusca.replace("{eid}", eid);
			
		} else {
			urlBusca = urlBusca.replace("{eid}", "null");
		}
		
		progressDialog.show();
		PesquisaFuncionario pesquisa = new PesquisaFuncionario();
		
		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
			pesquisa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, urlBusca);
			
		} else {
			pesquisa.execute(urlBusca);
			
		}
		
	}


	private class PesquisaFuncionario extends AsyncTask<String, Void, ArrayList<FuncionarioModel>>{

		@Override
		protected ArrayList<FuncionarioModel> doInBackground(String... params) {
			ArrayList<FuncionarioModel> funcionarios = new ArrayList<FuncionarioModel>();
			DefaultHttpClient httpClient = new DefaultHttpClient();
			
			try {
				
				HttpGet get = new HttpGet(params[0]);
				HttpResponse result = httpClient.execute(get);
				
				HttpEntity entity = result.getEntity();
				String resultString = EntityUtils.toString(entity, "utf-8");
				
				JSONArray resultJSON = new JSONArray(resultString);
				JSONObject funcionarioJSON;
				FuncionarioModel funcionarioModel;
				
				for(int indice = 0; indice < resultJSON.length(); indice++) {
					funcionarioJSON = (JSONObject) resultJSON.get(indice);
					funcionarioModel = new FuncionarioModel();
					
					if (funcionarioJSON.has("nome")) {
						funcionarioModel.setNome(funcionarioJSON.getString("nome"));
					}
					
					if (funcionarioJSON.has("eid")) {
						funcionarioModel.setEid(funcionarioJSON.getString("eid"));
					}
					
					if (funcionarioJSON.has("situacaoFuncionario")) {
						funcionarioModel.setSituacao(funcionarioJSON.getString("situacaoFuncionario"));
					}
					
				
					funcionarios.add(funcionarioModel);
				}
				
			} catch (MalformedURLException e) {
				Log.e("AsyncTask", e.getMessage());
			} catch (IOException e) {
				Log.e("AsyncTask", e.getMessage());
			} catch (JSONException e) {
				Log.e("AsyncTask", e.getMessage());
			}
			
			return funcionarios;
		}
		
		@Override
		protected void onPostExecute(ArrayList<FuncionarioModel> result) {
			super.onPostExecute(result);
			adapter.setLista(result);
			progressDialog.hide();
			
		}
		
		
	}


	@Override
	public void onClick(DialogInterface dialog, int which) {
		if (which == DialogInterface.BUTTON_POSITIVE) {
			iniciaBusca();
		} else {
			dialog.cancel();
		}
		
	}
}
