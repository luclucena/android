package com.example.minhascompras;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.exemple.minhascompras.componets.ListViewAdapter;
import com.exemple.minhascompras.models.ListViewModel;

public class ListViewActivity extends Activity {

	private ListView listView;
	private ArrayList<ListViewModel> listaModels;
	private ListViewAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_view);

		String stringjson = lerJson();
		tratarJson(stringjson);

		preencherLista();
		listView = (ListView) findViewById(R.id.listView1);
		adapter = new ListViewAdapter(listaModels, this);
		listView.setAdapter(adapter);
	}

	private void preencherLista() {
		listaModels = new ArrayList<ListViewModel>();
		ListViewModel modelo;

		for(int index = 0; index<= 25; index++){
			modelo = new ListViewModel();

			modelo.setText("Esta � a descri��o do item " + index);
			modelo.setImage(Long.valueOf(R.drawable.ic_launcher));

			listaModels.add(modelo);
		}
	}

	private void tratarJson(String stringArquivo){
		try {
			JSONObject objetoPrincipal = new JSONObject(stringArquivo);
			JSONObject objetoSecundario = objetoPrincipal.getJSONObject("object");
			JSONArray objetoArray = objetoPrincipal.getJSONArray("lista");

			boolean objectoBoolean = objetoPrincipal.getBoolean("booleanObject");
			int objetoNumerico = objetoPrincipal.getInt("numberObject");
			String objetoString = objetoPrincipal.getString("stringObject");

			if (objetoSecundario.has("campoA")){
				String campoA = objetoSecundario.getString("campoA");
			}

			String campoB = objetoSecundario.getString("campoB");
			String campoC = objetoSecundario.getString("campoC");

			for (int indice = 0; indice < objetoArray.length(); indice++){
				String nome = ((JSONObject) objetoArray.get(indice)).getString("nome");
				String descricao = ((JSONObject) objetoArray.get(indice)).getString("descricao");
			}


		} catch (JSONException e) {
			e.printStackTrace();
		}



	}

	private String lerJson(){
		AssetManager assetManager = this.getAssets();
		ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
		int letra = 0;

		try {
			InputStream inputStream = assetManager.open("json_exemple.txt");

			while (letra != -1) {
				byteOutputStream.write(letra);
				letra = inputStream.read();
			}
			inputStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return byteOutputStream.toString();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		switch(id) {
		case R.id.treinamento_list_view :
			Intent intent1 = new Intent(ListViewActivity.this, ListViewActivity.class);
			startActivity(intent1);
			break;

		case R.id.treinamento_lista_funcionarios :
			Intent intent2 = new Intent(ListViewActivity.this, ListFuncionarioActivity.class);
			startActivity(intent2);
			break;
			
		case R.id.treinamento_notificacao :
			Intent intent3 = new Intent(ListViewActivity.this, NotificacaoActivity.class);
			startActivity(intent3);
			break; 
			
		case R.id.treinamento_sharedpreferences :
			Intent intent4 = new Intent(ListViewActivity.this, SharedPreferencesActivity.class);
			startActivity(intent4);
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

}
